<?php

namespace Drupal\eca_parameters\Event;

/**
 * Events provided by the ECA Parameters module.
 */
final class EcaParametersEvents {

  /**
   * Dispatched when a parameter is being requested.
   *
   * @Event
   *
   * @var string
   */
  const REQUEST = 'eca_parameters.request';

}
