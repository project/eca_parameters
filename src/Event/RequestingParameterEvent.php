<?php

namespace Drupal\eca_parameters\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Entity\EntityInterface;
use Drupal\eca\Event\ConditionalApplianceInterface;
use Drupal\eca\Plugin\DataType\DataTransferObject;
use Drupal\eca\Token\DataProviderInterface;
use Drupal\parameters\Event\CollectionsPreparationEvent;

/**
 * Event for requesting a parameter.
 *
 * @internal
 *   This class is not meant to be used as a public API. It is subject for name
 *   change or may be removed completely, also on minor version updates.
 */
class RequestingParameterEvent extends Event implements ConditionalApplianceInterface, DataProviderInterface {

  /**
   * The original collections preparation event.
   *
   * @var \Drupal\parameters\Event\CollectionsPreparationEvent
   */
  protected CollectionsPreparationEvent $event;

  /**
   * An instance holding event data accessible as Token.
   *
   * @var \Drupal\eca\Plugin\DataType\DataTransferObject|null
   */
  protected ?DataTransferObject $eventData = NULL;

  /**
   * Constructs a new RequestingParameterEvent object.
   *
   * @param \Drupal\parameters\Event\CollectionsPreparationEvent $event
   *   The original collections preparation event.
   */
  public function __construct(CollectionsPreparationEvent $event) {
    $this->event = $event;
  }

  /**
   * {@inheritdoc}
   */
  public function getData(string $key) {
    if ($key === 'event') {
      if (!isset($this->eventData)) {
        $this->eventData = DataTransferObject::create([
          'machine-name' => EcaParametersEvents::REQUEST,
          'parameter-name' => $this->event->name,
        ]);
      }

      return $this->eventData;
    }
    elseif (isset($this->event->context[$key]) && ($this->event->context[$key] instanceof EntityInterface)) {
      return $this->event->context[$key];
    }

    foreach ($this->event->context as $value) {
      if ($value instanceof EntityInterface) {
        if (($key === 'entity') || ($key === $value->getEntityTypeId())) {
          return $value;
        }
      }
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function hasData(string $key): bool {
    return $this->getData($key) !== NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function appliesForLazyLoadingWildcard(string $wildcard): bool {
    if ($wildcard === $this->event->name) {
      return TRUE;
    }
    if (($wildcard === '') || ($wildcard === '*')) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(string $id, array $arguments): bool {
    if (isset($arguments['parameter_name'])) {
      if ($arguments['parameter_name'] === $this->event->name) {
        return TRUE;
      }
      if (in_array($arguments['parameter_name'], ['', '*'])) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Get the original collections preparation event.
   *
   * @return \Drupal\parameters\Event\CollectionsPreparationEvent
   *   The preparation event.
   */
  public function getPreparationEvent(): CollectionsPreparationEvent {
    return $this->event;
  }

}
