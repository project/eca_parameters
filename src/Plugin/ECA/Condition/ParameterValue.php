<?php

namespace Drupal\eca_parameters\Plugin\ECA\Condition;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\eca\Plugin\ECA\Condition\ConditionBase;
use Drupal\eca\Plugin\ECA\Condition\StringComparisonBase;
use Drupal\parameters\Entity\ParametersCollection;
use Drupal\parameters\Entity\ParametersCollectionStorage;
use Drupal\parameters\ParameterRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the ECA condition for key value store values.
 *
 * @EcaCondition(
 *   id = "eca_parameter_value",
 *   label = @Translation("Parameter: compare value"),
 *   context_definitions = {
 *     "entity" = @ContextDefinition("entity", label = @Translation("Entity"))
 *   }
 * )
 */
class ParameterValue extends StringComparisonBase {

  /**
   * {@inheritdoc}
   */
  protected static bool $replaceTokens = FALSE;

  /**
   * The parameter repository.
   *
   * @var \Drupal\parameters\ParameterRepository
   */
  protected ParameterRepository $parameterRepository;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): ConditionBase {
    /** @var \Drupal\eca_parameters\Plugin\ECA\Condition\ParameterValue $instance */
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->setParameterRepository($container->get(ParameterRepository::SERVICE_NAME));
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function getLeftValue(): string {
    $entity = $this->getValueFromContext('entity');
    $name = (string) $this->tokenServices->replace($this->configuration['parameter_name']);
    return (string) $this->parameterRepository->getParameter($name, $entity)->getProcessedData()->getString();
  }

  /**
   * {@inheritdoc}
   */
  protected function getRightValue(): string {
    return $this->tokenServices->replace($this->configuration['value']);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'parameter_name' => '',
      'value' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $available = [];
    /** @var \Drupal\parameters\Entity\ParametersCollectionInterface $collection */
    foreach (ParametersCollectionStorage::get()->loadMultiple() as $collection) {
      $parameters = $collection->getParameters();
      if (!$parameters->count()) {
        continue;
      }
      $id = $collection->id();
      /** @var \Drupal\parameters\Plugin\ParameterInterface $parameter */
      foreach ($parameters as $name => $parameter) {
        $available[] = '<strong>' . $id . ':' . $name . '</strong>';
      }
    }
    $available = $available ? Markup::create('<ul><li>' . implode('</li><li>', $available) . '</li></ul>') : $this->t('No parameters have been defined via configuration yet.');
    $form['parameter_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Parameter name'),
      '#description' => $this->t('The machine name of the parameter. Examples: <em>my_eca_parameter</em>, <em>global:my_global_param</em>, <em>node.article:my_article_param</em>. Available parameters: @available', [
        '@available' => $available,
      ]),
      '#default_value' => $this->configuration['parameter_name'],
      '#weight' => -60,
    ];
    $form['value'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Value'),
      '#description' => $this->t('This field supports tokens.'),
      '#default_value' => $this->configuration['value'],
      '#weight' => -70,
    ];
    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->configuration['parameter_name'] = $form_state->getValue('parameter_name');
    $this->configuration['value'] = $form_state->getValue('value');
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * Set the parameter repository.
   *
   * @param \Drupal\parameters\ParameterRepository $repository
   *   The repository.
   */
  public function setParameterRepository(ParameterRepository $repository): void {
    $this->parameterRepository = $repository;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies(): array {
    $dependencies = parent::calculateDependencies();
    if (strpos($this->configuration['parameter_name'], ':')) {
      $parts = explode(':', $this->configuration['parameter_name'], 2);
      if ($collection = ParametersCollection::load(reset($parts))) {
        $dependencies[$collection->getConfigDependencyKey()][] = $collection->getConfigDependencyName();
      }
    }
    return $dependencies;
  }

}
