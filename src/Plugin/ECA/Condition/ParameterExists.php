<?php

namespace Drupal\eca_parameters\Plugin\ECA\Condition;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\eca\Plugin\ECA\Condition\ConditionBase;
use Drupal\parameters\Entity\ParametersCollectionStorage;
use Drupal\parameters\Exception\ParameterNotFoundException;
use Drupal\parameters\ParameterRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Condition plugin to check whether a parameter exists.
 *
 * @EcaCondition(
 *   id = "eca_parameter_exists",
 *   label = @Translation("Parameter: exists"),
 *   context_definitions = {
 *     "entity" = @ContextDefinition("entity", label = @Translation("Entity"))
 *   }
 * )
 */
class ParameterExists extends ConditionBase {

  /**
   * The parameter repository.
   *
   * @var \Drupal\parameters\ParameterRepository
   */
  protected ParameterRepository $parameterRepository;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): ConditionBase {
    /** @var \Drupal\eca_parameters\Plugin\ECA\Condition\ParameterExists $instance */
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->setParameterRepository($container->get(ParameterRepository::SERVICE_NAME));
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'parameter_name' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);
    $available = [];
    /** @var \Drupal\parameters\Entity\ParametersCollectionInterface $collection */
    foreach (ParametersCollectionStorage::get()->loadMultiple() as $collection) {
      $parameters = $collection->getParameters();
      if (!$parameters->count()) {
        continue;
      }
      $id = $collection->id();
      /** @var \Drupal\parameters\Plugin\ParameterInterface $parameter */
      foreach ($parameters as $name => $parameter) {
        $available[] = '<strong>' . $id . ':' . $name . '</strong>';
      }
    }
    $available = $available ? Markup::create('<ul><li>' . implode('</li><li>', $available) . '</li></ul>') : $this->t('No parameters have been defined via configuration yet.');
    $form['parameter_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Parameter name'),
      '#description' => $this->t('The machine name of the parameter. Examples: <em>my_eca_parameter</em>, <em>global:my_global_param</em>, <em>node.article:my_article_param</em>. Available parameters: @available', [
        '@available' => $available,
      ]),
      '#default_value' => $this->configuration['parameter_name'],
      '#weight' => -20,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->configuration['parameter_name'] = $form_state->getValue('parameter_name');
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate(): bool {
    $entity = $this->getValueFromContext('entity');
    $name = (string) $this->tokenServices->replace($this->configuration['parameter_name']);
    try {
      $parameter = $this->parameterRepository->getParameter($name, $entity);
    }
    catch (ParameterNotFoundException $e) {
      $parameter = NULL;
    }
    return $this->negationCheck(!empty($parameter));
  }

  /**
   * Set the parameter repository.
   *
   * @param \Drupal\parameters\ParameterRepository $repository
   *   The repository.
   */
  public function setParameterRepository(ParameterRepository $repository): void {
    $this->parameterRepository = $repository;
  }

}
