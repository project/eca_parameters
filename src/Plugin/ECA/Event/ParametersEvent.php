<?php

namespace Drupal\eca_parameters\Plugin\ECA\Event;

use Drupal\Core\Form\FormStateInterface;
use Drupal\eca\Entity\Objects\EcaEvent;
use Drupal\eca\Event\Tag;
use Drupal\eca\Plugin\ECA\Event\EventBase;
use Drupal\eca_parameters\Event\EcaParametersEvents;
use Drupal\eca_parameters\Event\RequestingParameterEvent;

/**
 * Plugin implementation of the ECA base Events.
 *
 * @EcaEvent(
 *   id = "parameters",
 *   deriver = "Drupal\eca_parameters\Plugin\ECA\Event\ParametersEventDeriver"
 * )
 */
class ParametersEvent extends EventBase {

  /**
   * {@inheritdoc}
   */
  public static function definitions(): array {
    $definitions = [];
    $definitions['request'] = [
      'label' => 'Requesting parameter',
      'event_name' => EcaParametersEvents::REQUEST,
      'event_class' => RequestingParameterEvent::class,
      'tags' => Tag::READ | Tag::BEFORE | Tag::CONFIG,
    ];
    return $definitions;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    if ($this->eventClass() === RequestingParameterEvent::class) {
      $values = [
        'parameter_name' => '',
      ];
    }
    else {
      $values = [];
    }
    return $values + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);
    if ($this->eventClass() === RequestingParameterEvent::class) {
      $form['parameter_name'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Parameter name'),
        '#description' => $this->t('The machine name of the requested parameter. Examples: <em>my_eca_parameter</em>, <em>my_global_param</em>, <em>my_article_param</em>.'),
        '#default_value' => $this->configuration['parameter_name'],
        '#required' => TRUE,
        '#weight' => -20,
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    parent::submitConfigurationForm($form, $form_state);
    if ($this->eventClass() === RequestingParameterEvent::class) {
      $this->configuration['parameter_name'] = $form_state->getValue('parameter_name');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function lazyLoadingWildcard(string $eca_config_id, EcaEvent $ecaEvent): string {
    switch ($this->getDerivativeId()) {

      case 'request':
        $configuration = $ecaEvent->getConfiguration();
        return isset($configuration['parameter_name']) ? trim($configuration['parameter_name']) : '*';

      default:
        return parent::lazyLoadingWildcard($eca_config_id, $ecaEvent);

    }
  }

}
