<?php

namespace Drupal\eca_parameters\Plugin\ECA\Event;

use Drupal\eca\Plugin\ECA\Event\EventDeriverBase;

/**
 * Deriver for ECA Parameters event plugins.
 */
class ParametersEventDeriver extends EventDeriverBase {

  /**
   * {@inheritdoc}
   */
  protected function definitions(): array {
    return ParametersEvent::definitions();
  }

}
