<?php

namespace Drupal\eca_parameters\Plugin\Action;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\eca\Plugin\Action\ActionBase;
use Drupal\eca\Plugin\Action\ConfigurableActionBase;
use Drupal\eca\Service\YamlParser;
use Drupal\parameters\Entity\ParametersCollectionInterface;
use Drupal\parameters\Entity\ParametersCollectionStorage;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Yaml\Exception\ParseException;

/**
 * Action to set a parameter on runtime.
 *
 * @Action(
 *   id = "eca_parameter_set",
 *   label = @Translation("Set parameter"),
 *   description = @Translation("Set a parameter on the ECA parameters collection.")
 * )
 */
class ParameterSet extends ConfigurableActionBase {

  /**
   * The parameters collection storage.
   *
   * @var \Drupal\parameters\Entity\ParametersCollectionStorage
   */
  protected EntityStorageInterface $collectionStorage;

  /**
   * The YAML parser.
   *
   * @var \Drupal\eca\Service\YamlParser
   */
  protected YamlParser $yamlParser;

  /**
   * The parameters collection ID to use.
   *
   * @var string
   */
  static protected string $collectionId = 'eca';

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): ActionBase {
    /** @var \Drupal\eca_parameters\Plugin\Action\ParameterSet $instance */
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->setCollectionStorage($container->get('entity_type.manager')->getStorage(ParametersCollectionInterface::ENTITY_TYPE_ID));
    $instance->setYamlParser($container->get('eca.service.yaml_parser'));
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function execute(): void {
    $token = $this->tokenServices;
    $name = (string) $token->replace($this->configuration['parameter_name']);

    $collection = ParametersCollectionStorage::$cachedCollections[static::$collectionId] ?? $this->collectionStorage->load(static::$collectionId);
    if (!$collection) {
      $collection = $this->collectionStorage->create([
        'id' => static::$collectionId,
        'label' => ucfirst(static::$collectionId),
        'langcode' => 'en',
        'status' => TRUE,
        'locked' => TRUE,
        'parameters' => [],
      ]);
    }

    // Ensure the usage of the same collection object on runtime, by adding it
    // to the list of statically cached collections.
    if (isset(ParametersCollectionStorage::$cachedCollections) && !isset(ParametersCollectionStorage::$cachedCollections[static::$collectionId])) {
      ParametersCollectionStorage::$cachedCollections[static::$collectionId] = $collection;
    }

    /** @var \Drupal\parameters\Entity\ParametersCollectionInterface $collection */
    $parameters_array = $collection->get('parameters');
    if (isset($parameters_array[$name])) {
      $parameters_array[$name] = array_intersect_key($parameters_array[$name], [
        'label' => 1,
        'description' => 1,
        'weight' => 1,
        'third_party_settings' => 1,
      ]);
    }
    else {
      $parameters_array[$name] = [
        'label' => str_replace('_', ' ', ucfirst($name)),
        'description' => '',
        'weight' => count($parameters_array) + 10,
        'third_party_settings' => [],
      ];
    }

    $parameters_array[$name]['name'] = $name;
    $value = $this->configuration['parameter_value'];

    if ($this->configuration['use_yaml']) {
      try {
        $value = $this->yamlParser->parse($value);
      }
      catch (ParseException $e) {
        \Drupal::logger('eca')->error('Tried parsing a Parameter value in action "eca_parameter_set" as YAML format, but parsing failed.');
        return;
      }
      $parameters_array[$name]['type'] = 'yaml';
      $parameters_array[$name]['values'] = $value;
    }
    else {
      $value = $token->replace($value);
      $parameters_array[$name]['type'] = ctype_digit($value) ? 'integer' : 'string';
      $parameters_array[$name]['value'] = $value;
    }

    $parameters_changed = ($parameters_array !== $collection->get('parameters'));
    if ($this->configuration['save'] && $parameters_changed) {
      // Load an unchanged variant and save that one, in order to avoid
      // accidentally saving other runtime parameters.
      /** @var \Drupal\parameters\Entity\ParametersCollectionInterface $unchanged */
      if (!($unchanged = $this->collectionStorage->loadUnchanged(static::$collectionId))) {
        if (!$collection->isNew()) {
          throw new \RuntimeException(sprintf("The %s collection does not exist.", static::$collectionId));
        }
        $unchanged = $collection;
      }
      $unchanged->setParameters([
        $name => $parameters_array[$name],
      ] + ($unchanged->get('parameters') ?? []));
      $unchanged->save();
      // Load the collection again, to set the most recent parameters and
      // get the other runtime parameters back in.
      /** @var \Drupal\parameters\Entity\ParametersCollectionInterface $collection */
      $collection = ParametersCollectionStorage::$cachedCollections[static::$collectionId] ?? $this->collectionStorage->load(static::$collectionId);
    }
    $collection->setParameters($parameters_array);
    // Enforce the collection to be locked on runtime. This prevents redundant
    // save calls when the parameter will be requested later on.
    $collection->setLocked(TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'parameter_name' => '',
      'parameter_value' => '',
      'use_yaml' => FALSE,
      'save' => FALSE,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['parameter_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Parameter name'),
      '#description' => $this->t('The machine name of the parameter. Example: <em>my_eca_parameter</em>'),
      '#default_value' => $this->configuration['parameter_name'],
      '#weight' => -20,
    ];
    $form['parameter_value'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Value of the parameter'),
      '#default_value' => $this->configuration['parameter_value'],
      '#weight' => -10,
    ];
    $form['use_yaml'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Interpret above value as YAML format'),
      '#description' => $this->t('Nested data can be set using YAML format, for example <em>mykey: "My value"</em>. When using this format, this option needs to be enabled. When using tokens and YAML altogether, make sure that tokens are wrapped as a string. Example: <em>title: "[node:title]"</em>'),
      '#default_value' => $this->configuration['use_yaml'],
      '#weight' => -5,
    ];
    $form['save'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Save according collection'),
      '#description' => $this->t('When enabled, the configuration will be saved permanently with the newly set value. If not enabled, the set value only persists on runtime.'),
      '#default_value' => $this->configuration['save'],
      '#weight' => -5,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->configuration['parameter_name'] = $form_state->getValue('parameter_name');
    $this->configuration['parameter_value'] = $form_state->getValue('parameter_value');
    $this->configuration['use_yaml'] = $form_state->getValue('use_yaml');
    $this->configuration['save'] = $form_state->getValue('save');
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * Set the parameters collections storage.
   *
   * @param \Drupal\parameters\Entity\ParametersCollectionStorage $storage
   *   The collection storage.
   */
  public function setCollectionStorage(EntityStorageInterface $storage) {
    $this->collectionStorage = $storage;
  }

  /**
   * Set the YAML parser.
   *
   * @param \Drupal\eca\Service\YamlParser $yaml_parser
   *   The YAML parser.
   */
  public function setYamlParser(YamlParser $yaml_parser): void {
    $this->yamlParser = $yaml_parser;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies(): array {
    $dependencies = parent::calculateDependencies();
    if ($collection = $this->collectionStorage->load(static::$collectionId)) {
      $dependencies[$collection->getConfigDependencyKey()][] = $collection->getConfigDependencyName();
    }
    return $dependencies;
  }

}
