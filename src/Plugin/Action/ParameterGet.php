<?php

namespace Drupal\eca_parameters\Plugin\Action;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\eca\Plugin\Action\ActionBase;
use Drupal\eca\Plugin\Action\ConfigurableActionBase;
use Drupal\parameters\Entity\ParametersCollection;
use Drupal\parameters\Entity\ParametersCollectionStorage;
use Drupal\parameters\ParameterRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Action to read value from a parameter and store it as a token.
 *
 * @Action(
 *   id = "eca_parameter_get",
 *   label = @Translation("Get parameter"),
 *   description = @Translation("Get a parameter and store its value as a token."),
 *   type = "entity"
 * )
 */
class ParameterGet extends ConfigurableActionBase {

  /**
   * The parameter repository.
   *
   * @var \Drupal\parameters\ParameterRepository
   */
  protected ParameterRepository $parameterRepository;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): ActionBase {
    /** @var \Drupal\eca_parameters\Plugin\Action\ParameterGet $instance */
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->setParameterRepository($container->get(ParameterRepository::SERVICE_NAME));
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL): void {
    $name = (string) $this->tokenServices->replace($this->configuration['parameter_name']);
    $parameter = $this->parameterRepository->getParameter($name, $entity);
    $this->tokenServices->addTokenData($this->configuration['token_name'], $parameter->getProcessedData());
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'parameter_name' => '',
      'token_name' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);
    $available = [];
    /** @var \Drupal\parameters\Entity\ParametersCollectionInterface $collection */
    foreach (ParametersCollectionStorage::get()->loadMultiple() as $collection) {
      $parameters = $collection->getParameters();
      if (!$parameters->count()) {
        continue;
      }
      $id = $collection->id();
      /** @var \Drupal\parameters\Plugin\ParameterInterface $parameter */
      foreach ($parameters as $name => $parameter) {
        $available[] = '<strong>' . $id . ':' . $name . '</strong>';
      }
    }
    $available = $available ? Markup::create('<ul><li>' . implode('</li><li>', $available) . '</li></ul>') : $this->t('No parameters have been defined via configuration yet.');
    $form['parameter_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Parameter name'),
      '#description' => $this->t('The machine name of the parameter. Examples: <em>my_eca_parameter</em>, <em>global:my_global_param</em>, <em>node.article:my_article_param</em>. Available parameters: @available', [
        '@available' => $available,
      ]),
      '#default_value' => $this->configuration['parameter_name'],
      '#weight' => -20,
    ];
    $form['token_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name of token'),
      '#default_value' => $this->configuration['token_name'],
      '#weight' => -10,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->configuration['parameter_name'] = $form_state->getValue('parameter_name');
    $this->configuration['token_name'] = $form_state->getValue('token_name');
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * Set the parameter repository.
   *
   * @param \Drupal\parameters\ParameterRepository $repository
   *   The repository.
   */
  public function setParameterRepository(ParameterRepository $repository): void {
    $this->parameterRepository = $repository;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies(): array {
    $dependencies = parent::calculateDependencies();
    if (strpos($this->configuration['parameter_name'], ':')) {
      $parts = explode(':', $this->configuration['parameter_name'], 2);
      if ($collection = ParametersCollection::load(reset($parts))) {
        $dependencies[$collection->getConfigDependencyKey()][] = $collection->getConfigDependencyName();
      }
    }
    return $dependencies;
  }

}
