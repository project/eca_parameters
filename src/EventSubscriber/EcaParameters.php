<?php

namespace Drupal\eca_parameters\EventSubscriber;

use Drupal\eca\EventSubscriber\EcaBase;
use Drupal\eca_parameters\Event\EcaParametersEvents;
use Drupal\eca_parameters\Event\RequestingParameterEvent;
use Drupal\eca_parameters\Plugin\ECA\Event\ParametersEvent;
use Drupal\parameters\Entity\ParametersCollectionInterface;
use Drupal\parameters\Event\CollectionsPreparationEvent;
use Drupal\parameters\Event\ParameterEvents;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * ECA Parameters event subscriber.
 */
class EcaParameters extends EcaBase {

  /**
   * The parameters collection ID to additionally use.
   *
   * @var string|null
   */
  protected static string $collectionId = 'eca';

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected EventDispatcherInterface $eventDispatcher;

  /**
   * Invoked when the collection preparation event dispatches.
   *
   * @param mixed $event
   *   The event.
   */
  public function onPreparation($event): void {
    if (!($event instanceof CollectionsPreparationEvent)) {
      return;
    }
    if (isset($event->name)) {
      // A parameter name is specified, dispatch an explicit event for it.
      $this->eventDispatcher->dispatch(new RequestingParameterEvent($event), EcaParametersEvents::REQUEST);
    }
    // Place the ECA collection into the list of available collections.
    // It must be of higher priority than the global collection.
    if (isset(static::$collectionId) && ($collection = $this->entityTypeManager->getStorage(ParametersCollectionInterface::ENTITY_TYPE_ID)->load(static::$collectionId))) {
      $global_collection = NULL;
      if ($last_collection = end($event->collections)) {
        if ($last_collection->id() === 'global') {
          $global_collection = array_pop($event->collections);
        }
      }
      if (!in_array($collection, $event->collections, TRUE)) {
        array_push($event->collections, $collection);
      }
      if ($global_collection) {
        array_push($event->collections, $global_collection);
      }
      // Make sure the collections list starts at the beginning later on.
      reset($event->collections);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = [];
    $events[ParameterEvents::COLLECTIONS_PREPARATION][] = ['onPreparation'];
    foreach (ParametersEvent::definitions() as $definition) {
      $events[$definition['event_name']][] = ['onEvent'];
    }
    return $events;
  }

  /**
   * Set the event dispatcher.
   *
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $dispatcher
   *   The event dispatcher.
   */
  public function setEventDispatcher(EventDispatcherInterface $dispatcher): void {
    $this->eventDispatcher = $dispatcher;
  }

}
