# ECA Parameters

Integrates ECA: Event - Condition - Action with Parameters.

## 0. Contents

1. Introduction
2. Requirements
3. Installation
4. Usage

## 1. Introduction

This module provides events, conditions and actions when working with
parameters within ECA. It also provides a new collection of parameters for ECA.

## 2. Requirements

This module requires besides Drupal core the latest version of ECA: Event -
Condition - Action and Parameters.

## 3. Installation

Install the module as you would normally install a contributed
Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.

## 4. Usage

Once installed, you have the following plugins available within ECA:
- An event "Requesting parameter" that is fired up when parameters are being
  requested for usage.
- Conditions "Parameter: exists" and "Parameter: compare value".
- An action "Get parameter" for loading a parameter and storing it as a token.
- An action "Set parameter" for setting a parameter on runtime.

This module contains a further sub-module "ECA Parameters UI" to manage ECA
parameters via web user interface. When this sub-module is installed, a new menu
tab "Parameters" is available at `/admin/config/workflow/eca`.
